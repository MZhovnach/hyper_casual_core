﻿using UnityEngine;
using System.Collections.Generic;
using TObject.Shared;

namespace Core
{
    public class Localer : MonoBehaviour
    {
        /*
        http://docs.unity3d.com/ScriptReference/SystemLanguage.html
        http://docs.unity3d.com/Manual/StyledText.html
        */

        private static Dictionary<string, string> _textBase;
        private static string _defaultLocale = "English";

        public static void Init()
        {
            _textBase = new Dictionary<string, string>();
            Reload(GetSistemLanguage());
        }

        public static void Reload(string locale = "English")
        {
            if (_textBase == null)
            {
                _textBase = new Dictionary<string, string>();
            }

            TextAsset _localeString = Resources.Load<TextAsset>("Locales/" + locale + "/text/text");

            if (_localeString == null)
            {
                Debug.LogWarning("CAN'T FIND LOCALE '" + locale + "'. LOADING DEFAULT LOCALE '" + _defaultLocale + "'.");
                _localeString = Resources.Load<TextAsset>("Locales/" + _defaultLocale + "/text/text");
            }

            NanoXMLDocument document = new NanoXMLDocument(_localeString.text);
            NanoXMLNode RotNode = document.RootNode;

            foreach (NanoXMLNode node in RotNode.SubNodes)
            {
                if (node.Name.Equals("String"))
                {
                    string key = node.GetAttribute("id").Value.ToLower();
                    if (_textBase.ContainsKey(key))
                    {
                        Debug.LogWarning("Locale " + locale + " already contains " + key);
                    }
                    else
                    {
                        _textBase.Add(key, NormalizeDataString(node.Value));
                    }
                }
            }
        }

        public static string GetText(string id)
        {
            string key = id.ToLower();
            if (_textBase != null && _textBase.ContainsKey(key))
            {
                return _textBase[key];
            }
            else
            {
                return "#" + id + "#";
            }
        }

        public static string GetSistemLanguage()
        {
            return Application.systemLanguage.ToString();
        }

        public static string GetDefaultLocale()
        {
            return _defaultLocale;
        }

        private static string NormalizeDataString(string ampersandTaggetString)
        {
            ampersandTaggetString = ampersandTaggetString.Replace("&lt;", "<");
            ampersandTaggetString = ampersandTaggetString.Replace("&gt;", ">");
            ampersandTaggetString = ampersandTaggetString.Replace("&#13;", "\n");
            ampersandTaggetString = ampersandTaggetString.Replace("\r", "\n");
            return ampersandTaggetString;
        }
    }
}
