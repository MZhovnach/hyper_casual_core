// Mykhailo Zhovnach, BrainBlenderGames (c) 2018
// EventManager is a partial class of UltraCasualGameCore library
// Here is core game events that are common for all UCGs

namespace Core
{
    public partial class EventManager
    {
        public static event EventController.MethodContainer OnSceneLoadEvent;
        public void CallOnSceneLoadEvent(EventData ob = null) { if (OnSceneLoadEvent != null) OnSceneLoadEvent(ob); }

        public static event EventController.MethodContainer OnUpdateLevelProgressPanelEvent;
        public void CallOnUpdateLevelProgressPanelEvent(EventData ob = null) { if (OnUpdateLevelProgressPanelEvent != null) OnUpdateLevelProgressPanelEvent(ob); }

        public static event EventController.MethodContainer OnShowAwsomeTextEvent;
        public void CallOnShowAwsomeTextEvent(EventData ob = null) { if (OnShowAwsomeTextEvent != null) OnShowAwsomeTextEvent(ob); }

        public static event EventController.MethodContainer OnChangeZoneEvent;
        public void CallOnChangeZoneEvent(EventData ob = null) { if (OnChangeZoneEvent != null) OnChangeZoneEvent(ob); }
    }
}