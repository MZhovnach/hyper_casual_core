// Mykhailo Zhovnach, BrainBlenderGames (c) 2018
// UserData is a class of UltraCasualGameCore library
// You could define data to be saved between game sessions
// Think of this class as of UserPrefs

using UnityEngine;

namespace Core
{
    [System.Serializable]
    public partial class UserData
    {
        public float MusicVolume;
        public float SoundVolume;
        public int LevelNumber;

        public UserData()
        {
            MusicVolume = 0.2f;
            SoundVolume = 0.5f;
            LevelNumber = 1;
            Init();
        }

        partial void Init();
    }


    public class UserDataManager
    {
        public UserData Data;

        // save to player prefs
        public void Save()
        {
            string zippedJson = GZipHelper.Compress(JsonUtility.ToJson(Data));
            PlayerPrefs.SetString("SavedData", zippedJson);
            PlayerPrefs.SetInt("SavedVersion", Consts.APP_VERSION);
            PlayerPrefs.Save();
        }

        // load from player prefs
        public void Load()
        {
            int savedVersion = PlayerPrefs.GetInt("SavedVersion", -1);
            if (PlayerPrefs.HasKey("SavedData") && savedVersion == Consts.APP_VERSION)
            {
                string unZippedJson = GZipHelper.Decompress(PlayerPrefs.GetString("SavedData", ""));
                Data = JsonUtility.FromJson<UserData>(unZippedJson);
            }
            else
            {
                Data = new UserData();
                Save();
            }
        }
    }
}