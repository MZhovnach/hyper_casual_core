﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Core
{
    public partial class GameManager : Singleton<GameManager>
    {
        public UserDataManager Player;
        public EventManager Events;

        private bool _initialized = false;

        // guarantee this will be always a singleton only - can't use the constructor!
        protected GameManager()
        {
            _initialized = false;
        }

        public void BaseInitialize()
        {
            Application.targetFrameRate = 60;
            Localer.Init();
            Input.multiTouchEnabled = false;
            Player = new UserDataManager();
            Events = new EventManager();
            MusicManager.InitMusicManager();
            _initialized = true;
        }

        partial void GameInitialize();

        public void Initialize()
        {
            BaseInitialize();
            GameInitialize();
        }

        public bool Initialized()
        {
            return _initialized;
        }

        new public void OnDestroy()
        {
            base.OnDestroy();
        }

        public void Load()
        {
            Player.Load();
        }
    }
}