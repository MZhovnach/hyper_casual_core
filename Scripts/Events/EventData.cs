﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Core
{
    public class EventData
    {
        public Dictionary<string, object> Data;

        public EventData()
        {
            Data = new Dictionary<string, object>();
        }

        public void Log()
        {
            string logStr = "Event (";
            int keyIndex = 0;
            foreach (var kvp in Data)
            {
                keyIndex++;
                logStr += kvp.Key + ":" + kvp.Value;
                if (keyIndex < Data.Count)
                {
                    logStr += " ";
                }
            }
            logStr += ")";
            Debug.Log(logStr);
        }
    }
}
