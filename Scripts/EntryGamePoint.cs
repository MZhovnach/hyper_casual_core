﻿// Mykhailo Zhovnach, BrainBlenderGames (c) 2018
// EntryGamePoint is a class of UltraCasualGameCore library
// Here is where all the stuff are initialized

using UnityEngine;

#if ENABLE_ANALYTICS
using GameAnalyticsSDK;
using Facebook.Unity;
#endif

namespace Core
{
    public class EntryGamePoint : MonoBehaviour
    {

        // Use this for initialization
        void Awake()
        {
            if (!GameManager.Instance.Initialized())
            {
                GameManager.Instance.Initialize();
                GameManager.Instance.Load();
#if ENABLE_ANALYTICS
            GameAnalytics.Initialize();
            if (!FB.IsInitialized)
            {
                // Initialize the Facebook SDK
                FB.Init(InitCallback, OnHideUnity);
            }
            else
            {
                // Already initialized, signal an app activation App Event
                FB.ActivateApp();
            }
#endif
            }
        }

        private void InitCallback()
        {
#if ENABLE_ANALYTICS
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
#endif
        }

        private void OnHideUnity(bool isGameShown)
        {
            if (!isGameShown)
            {
                // Pause the game - we will need to hide
                Time.timeScale = 0;
            }
            else
            {
                // Resume the game - we're getting focus again
                Time.timeScale = 1;
            }
        }
    }
}
